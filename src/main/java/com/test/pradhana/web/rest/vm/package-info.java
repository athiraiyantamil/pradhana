/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.pradhana.web.rest.vm;
